
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 03/10/2015 19:53:36
-- Generated from EDMX file: C:\Users\Rodion\Desktop\GrooveList\GrooveList\GrooveList.Database\GrooveListDb.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GrooveListDatabase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_PlaylistSong_Playlist]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PlaylistSong] DROP CONSTRAINT [FK_PlaylistSong_Playlist];
GO
IF OBJECT_ID(N'[dbo].[FK_PlaylistSong_Song]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PlaylistSong] DROP CONSTRAINT [FK_PlaylistSong_Song];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Playlists]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Playlists];
GO
IF OBJECT_ID(N'[dbo].[Songs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Songs];
GO
IF OBJECT_ID(N'[dbo].[PlaylistSong]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PlaylistSong];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Playlists'
CREATE TABLE [dbo].[Playlists] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Songs'
CREATE TABLE [dbo].[Songs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Artist] nvarchar(max)  NOT NULL,
    [Duration] nvarchar(max)  NOT NULL,
    [DateCreated] datetimeoffset  NOT NULL,
    [YoutubeURL] nvarchar(max)  NOT NULL,
    [Genres] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PlaylistSong'
CREATE TABLE [dbo].[PlaylistSong] (
    [Playlists_Id] int  NOT NULL,
    [Songs_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Playlists'
ALTER TABLE [dbo].[Playlists]
ADD CONSTRAINT [PK_Playlists]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Songs'
ALTER TABLE [dbo].[Songs]
ADD CONSTRAINT [PK_Songs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Playlists_Id], [Songs_Id] in table 'PlaylistSong'
ALTER TABLE [dbo].[PlaylistSong]
ADD CONSTRAINT [PK_PlaylistSong]
    PRIMARY KEY CLUSTERED ([Playlists_Id], [Songs_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Playlists_Id] in table 'PlaylistSong'
ALTER TABLE [dbo].[PlaylistSong]
ADD CONSTRAINT [FK_PlaylistSong_Playlist]
    FOREIGN KEY ([Playlists_Id])
    REFERENCES [dbo].[Playlists]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Songs_Id] in table 'PlaylistSong'
ALTER TABLE [dbo].[PlaylistSong]
ADD CONSTRAINT [FK_PlaylistSong_Song]
    FOREIGN KEY ([Songs_Id])
    REFERENCES [dbo].[Songs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlaylistSong_Song'
CREATE INDEX [IX_FK_PlaylistSong_Song]
ON [dbo].[PlaylistSong]
    ([Songs_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------