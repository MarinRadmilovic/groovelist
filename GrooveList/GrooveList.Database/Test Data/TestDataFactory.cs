﻿using System;
using System.Linq;

namespace GrooveList.Database.TestData
{
    public static class TestDataFactory
    {
        public static void Fill()
        {
            try
            {
                using (var db = new GrooveListDbContainer())
                {
                    if (!db.Songs.Any())
                    {
                        InsertUserData(db);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                throw new DatabaseNotFoundException();
            }
        }

        private static void InsertUserData(GrooveListDbContainer db)
        {
            new[]
                {
                    new Playlist()
                    {
                        Name = "Hip Hop",
                        Songs =
                            {
                                new Song
                                {
                                    Name = "i",
                                    Artist = "Kendrick Lamar",
                                    Duration = "4:42",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Hip Hop / Rap;",
                                    YoutubeURL = "https://youtu.be/8aShfolR6w8",
                                    
                                },
                                new Song
                                {
                                    Name = "Collard Greens",
                                    Artist = "SchoolBoy Q",
                                    Duration = "4:43",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Hip Hop / Rap;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=_L2vJEb6lVE",
                                    
                                },
                                new Song
                                {
                                    Name = "Stronger",
                                    Artist = "Kanye West",
                                    Duration = "4:26",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Hip Hop / Rap;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=PsO6ZnUZI0g",
                                    
                                },
                                new Song
                                {
                                    Name = "Can't hold us",
                                    Artist = "Macklemore & Ryan Lewis ",
                                    Duration = "4:25",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Hip Hop / Rap;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=xHRkHFxD-xY",
                                    
                                },
                                new Song
                                {
                                    Name = "The Monster",
                                    Artist = "Eminem ",
                                    Duration = "4:42",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Hip Hop / Rap;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=EHkozMIXZ8w",
                                    
                                },
                                new Song
                                {
                                    Name = "Sing For The Moment",
                                    Artist = "Eminem",
                                    Duration = "5:27",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Hip Hop / Rap;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=D4hAVemuQXY",
                                    
                                }
                            }
                        },

                     new Playlist()
                     {
                        Name = "Pop",
                        Songs =
                            {
                                new Song
                                {
                                    Name = "Thinking Out Loud",
                                    Artist = "Ed Sheeran",
                                    Duration = "4:42",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Pop (Popular music);",
                                    YoutubeURL = "https://www.youtube.com/watch?v=lp-EO5I60KA",
                                    
                                },
                                new Song
                                {
                                    Name = "Sugar",
                                    Artist = "Maroon 5",
                                    Duration = "5:01",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Pop (Popular music);",
                                    YoutubeURL = "https://www.youtube.com/watch?v=09R8_2nJtjg",
                                    
                                },
                                new Song
                                {
                                    Name = "Masterpiece",
                                    Artist = "Jessie j",
                                    Duration = "3:58",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Pop (Popular music);",
                                    YoutubeURL = "https://www.youtube.com/watch?v=PTOFEgJ9zzI",
                                    
                                },
                                new Song
                                {
                                    Name = "Take me to church",
                                    Artist = "Hozier",
                                    Duration = "4:02",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Pop (Popular music);",
                                    YoutubeURL = "https://www.youtube.com/watch?v=MYSVMgRr6pw",
                                    
                                },
                                new Song
                                {
                                    Name = "Uptown Funk",
                                    Artist = "Mike Ronson feat. Bruno Mars",
                                    Duration = "4:30",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Pop (Popular music);",
                                    YoutubeURL = "https://www.youtube.com/watch?v=OPf0YbXqDm0",
                                    
                                },
                                new Song
                                {
                                    Name = "Rude",
                                    Artist = "MAGIC!",
                                    Duration = "3:45",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Pop (Popular music);",
                                    YoutubeURL = "https://www.youtube.com/watch?v=PIh2xe4jnpk",
                                    
                                }
                            }
                        },
                        new Playlist()
                        {
                        Name = "Rock",
                        Songs =
                            {
                                new Song
                                {
                                    Name = "Come as you are",
                                    Artist = "Nirvana",
                                    Duration = "4:42",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Rock;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=vabnZ9-ex7o",
                                    
                                },
                                new Song
                                {
                                    Name = "Best of You",
                                    Artist = "Foo Fighters",
                                    Duration = "4:16",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Rock;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=h_L4Rixya64",
                                    
                                },
                                new Song
                                {
                                    Name = "Anybody Seen My Baby",
                                    Artist = "The Rolling Stones",
                                    Duration = "4:44",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Rock;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=BinwuzZVjnE",
                                    
                                },
                                new Song
                                {
                                    Name = "November Rain",
                                    Artist = "Guns 'n' Roses",
                                    Duration = "4:02",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Rock;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=8SbUC-UaAxE",
                                    
                                },
                                new Song
                                {
                                    Name = "Crazy",
                                    Artist = "Aerosmith",
                                    Duration = "6:15",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Rock;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=NMNgbISmF4I",
                                    
                                },
                                new Song
                                {
                                    Name = "Always",
                                    Artist = "Bon Jovi",
                                    Duration = "6:02",
                                    DateCreated = DateTimeOffset.Now,
                                    Genres = "Rock;",
                                    YoutubeURL = "https://www.youtube.com/watch?v=9BMwcO6_hyA",
                                    
                                }
                            }
                        }

                }.ToList()
                 .ForEach(p => db.Playlists.Add(p));
        }

        private class DatabaseNotFoundException : Exception
        {
            public DatabaseNotFoundException()
                : base("Database GrooveListDb wasn't found on localhost server.")
            {
            }
        }
    }
}
