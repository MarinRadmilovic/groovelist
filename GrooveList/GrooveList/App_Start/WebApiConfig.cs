﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace GrooveList
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            
            config.Routes.MapHttpRoute("AddSong", "api/songs/add", new {controller = "SongApi", action = "AddSong"});
            config.Routes.MapHttpRoute("GetAllSongs", "api/songs/all", new { controller = "SongApi", action = "GetAll" });
            config.Routes.MapHttpRoute("AddPlaylist", "api/playlist/add", new { controller = "PlaylistApi", action = "AddPlaylist" });
            config.Routes.MapHttpRoute("GetAllPlaylists", "api/playlist/all", new { controller = "PlaylistApi", action = "GetAll" });
            config.Routes.MapHttpRoute("AddSongToPlaylist", "api/songToPlaylist", new { controller = "SongApi", action = "SongToPlaylist" });
            config.Routes.MapHttpRoute("RemoveSong", "api/songs/remove", new { controller = "SongApi", action = "RemoveSong" });
            config.Routes.MapHttpRoute("GetSongById", "api/songs/get", new { controller = "SongApi", action = "GetSongById" });
            config.Routes.MapHttpRoute("EditSong", "api/song/edit", new { controller = "SongApi", action = "EditSong" });
            config.Routes.MapHttpRoute("RemoveFromPlaylist", "api/songs/removeFromPlaylist", new { controller = "SongApi", action = "RemoveFromPlaylist" });
            config.Routes.MapHttpRoute("RemovePlaylist", "api/playlist/remove", new { controller = "PlaylistApi", action = "RemovePlaylist" });

        }
    }
}
