﻿using GrooveList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using GrooveList.Infrastructure.Entities;
using GrooveList.Infrastructure.Services;
using GrooveList.Infrastructure.Queries;

namespace GrooveList.Controllers
{
    public class SongApiController : ApiController
    {
        SongServices songServices;
        SongQueries songQueries;
        public SongApiController()
        {
            songServices = new SongServices();
            songQueries = new SongQueries();
        }
        [HttpPost]
        public void AddSong(SongModel model) 
        {
            var song = songServices.AddSong(new Song { 
            
                Name = model.Name,
                Artist = model.Artist,
                Duration = model.Duration,
                YoutubeURL = model.YoutubeURL,
                Genres = model.Genres,
                DateCreated = DateTime.UtcNow
            });
        }
        [HttpGet]
        public List<Song> GetAll()
        {
            var songList = songQueries.GetAll();

            return songList;
        }
        [HttpPost]
        public bool SongToPlaylist(SongPlaylistModel model) 
        {
            return songServices.SongToPlaylist(model.SongId, model.PlaylistId);
        }

        [HttpPost]
        public void RemoveSong(int songId) 
        {
            songServices.RemoveSong(songId);
        }
        [HttpGet]
        public Song GetSongById(int songId)
        {
            return songServices.GetSongById(songId);
        }
        [HttpPost]
        public bool EditSong(Song song) 
        {
            return songServices.EditSong(song);
        }
        [HttpPost]
        public void RemoveFromPlaylist(SongPlaylistModel model) 
        {
            songServices.RemoveSongFromPlaylist(model.SongId, model.PlaylistId);
        }
    }
}
