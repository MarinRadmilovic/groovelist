﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using GrooveList.Infrastructure.Entities;
using GrooveList.Infrastructure.Services;
using GrooveList.Infrastructure.Queries;

namespace GrooveList.Controllers
{
    public class PlaylistApiController : ApiController
    {
        PlaylistServices playlistServices;
        PlaylistQueries playlistQueries;
        public PlaylistApiController()
        {
            playlistServices = new PlaylistServices();
            playlistQueries = new PlaylistQueries();
        }
        [HttpPost]
        public Playlist AddPlaylist(Playlist playlist)
        {
            return playlistServices.AddPlaylist(playlist);
        }

        public void RemovePlaylist(int playlistId) 
        {
            playlistServices.RemovePlaylist(playlistId);
        }

        public List<Playlist> GetAll() 
        {
            return playlistQueries.GetAll();
        }
    }
}
