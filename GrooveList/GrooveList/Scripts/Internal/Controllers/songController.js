﻿myApp.controller('songController', ['$scope', '$http', 'songService', function ($scope, $http, songService) {

    $scope.init = function () {
        $scope.setToDefault();
    }

    $scope.submitSong = function (song) {
       
        $scope.song.Duration = $scope.duration._data.minutes.toString() + ":" +$scope.duration._data.seconds.toString();
        $scope.song.Genres = "";
        angular.forEach($scope.genreGroups.genreGroups, function (group) {
            angular.forEach(group, function (genre) {
                if(genre.selected == true)
                $scope.song.Genres += genre.name + ";";
            })
        })
        console.log($scope.song);

        songService.addSong(song)
            .then(function (response) {

                $scope.setToDefault();
            });
    }


    $scope.setToDefault = function () {
        $scope.song = {
            "Id": 0,
            "YoutubeURL": "",
            "Name": "",
            "Artist": "",
            "Duration": "",
            "Genres": ""
        };
        $scope.duration = moment.duration({
            seconds: 00,
            minutes: 00,
            hours: 00,
            days: 00,
        });
        $scope.genreGroups = {
            "genreGroups": [
                    [
                        { "name": "Alternative Music", "selected": "false" },
                        { "name": "Blues", "selected": "false" },
                        { "name": "Classical Music", "selected": "false" },
                        { "name": "Country Music", "selected": "false" },
                        { "name": "Dance Music", "selected": "false" },
                        { "name": "Easy Listening", "selected": "false" },
                        { "name": "Electronic Music", "selected": "false" },
                        { "name": "European Music (Folk / Pop)", "selected": "false" },
                        { "name": "Hip Hop / Rap", "selected": "false" },
                        { "name": "Indie Pop", "selected": "false" },
                        { "name": "Inspirational (incl. Gospel)", "selected": "false" }
                    ],
                    [
                        { "name": "Asian Pop (J-Pop, K-pop)", "selected": "false" },
                        { "name": "Jazz", "selected": "false" },
                        { "name": "Latin Music", "selected": "false" },
                        { "name": "New Age", "selected": "false" },
                        { "name": "Opera", "selected": "false" },
                        { "name": "Pop (Popular music)", "selected": "false" },
                        { "name": "R&B / Soul", "selected": "false" },
                        { "name": "Reggae", "selected": "false" },
                        { "name": "Singer / Songwriter", "selected": "false" },
                        { "name": "Rock", "selected": "false" },
                        { "name": "World Music / Beats", "selected": "false" }
                    ]
            ]
        }
    }

    
       
}]);