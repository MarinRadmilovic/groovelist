﻿myApp.controller('editSongController', ['$scope', '$stateParams', 'songService', function ($scope, $stateParams, songService) {

    $scope.init = function () {

        $scope.isSongEditedWell = false;
        $scope.isSongNotEditedWell = false;

        songService.getSongById($stateParams.songId)
            .then(function (response) {
                $scope.song = response.data;
                var time = $scope.song.Duration.split(":");
                $scope.duration = moment.duration({
                    seconds: time[1],
                    minutes: time[0],
                    hours: 00,
                    days: 00,
                });

                var genres = $scope.song.Genres.split(';');
                genres.pop();

                angular.forEach(genres, function (genre) {                    
                    var group = _.find($scope.genreGroups.genreGroups, function (_group) {                     
                        return _.findWhere(_group, { 'name': genre, 'selected': 'false' });
                    });
                    _.find(group, function (_genre) {
                        return _genre.name == genre;
                    }).selected = true;
                })
                
               
            });
    
    };

    $scope.submitSong = function (song) {

        $scope.song.Duration = $scope.duration._data.minutes.toString() + ":" + $scope.duration._data.seconds.toString();
        $scope.song.Genres = "";
        angular.forEach($scope.genreGroups.genreGroups, function (group) {
            angular.forEach(group, function (genre) {
                if (genre.selected == true)
                    $scope.song.Genres += genre.name + ";";
            })
        })

        songService.editSong($scope.song)
            .then(function (response) {
                if (response.data) {
                    $scope.isSongEditedWell = true;
                    $scope.isSongNotEditedWell = false;
                }
                else{
                    $scope.isSongNotEditedWell = true;
                    $scope.isSongEditedWell = false;
                }
            });
    }

    $scope.genreGroups = {
        "genreGroups": [
                [
                    { "name": "Alternative Music", "selected": "false" },
                    { "name": "Blues", "selected": "false" },
                    { "name": "Classical Music", "selected": "false" },
                    { "name": "Country Music", "selected": "false" },
                    { "name": "Dance Music", "selected": "false" },
                    { "name": "Easy Listening", "selected": "false" },
                    { "name": "Electronic Music", "selected": "false" },
                    { "name": "European Music (Folk / Pop)", "selected": "false" },
                    { "name": "Hip Hop / Rap", "selected": "false" },
                    { "name": "Indie Pop", "selected": "false" },
                    { "name": "Inspirational (incl. Gospel)", "selected": "false" }
                ],
                [
                    { "name": "Asian Pop (J-Pop, K-pop)", "selected": "false" },
                    { "name": "Jazz", "selected": "false" },
                    { "name": "Latin Music", "selected": "false" },
                    { "name": "New Age", "selected": "false" },
                    { "name": "Opera", "selected": "false" },
                    { "name": "Pop (Popular music)", "selected": "false" },
                    { "name": "R&B / Soul", "selected": "false" },
                    { "name": "Reggae", "selected": "false" },
                    { "name": "Singer / Songwriter", "selected": "false" },
                    { "name": "Rock", "selected": "false" },
                    { "name": "World Music / Beats", "selected": "false" }
                ]
        ]
    }
    
}]);