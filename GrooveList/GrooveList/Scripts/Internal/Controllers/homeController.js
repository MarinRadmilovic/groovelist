﻿myApp.controller('homeController', ['$scope', 'songService','playlistService', function ($scope, songService, playlistService) {
    $scope.selected = -1;
    $scope.playlistsLoaded = false;
    $scope.songsLoaded = false;

    $scope.init = function () {


        songService.getAllSongs()
                   .then(function (response) {
                       $scope.songs = response.data;
                       $scope.songsLoaded = true;
                   });
        playlistService.getAllPlaylists()
                    .then(function (response) {
                        $scope.playlists = response.data;
                        $scope.playlistsLoaded = true;
                    });
    }
    
    $scope.playlistOpened = function (index, playlist, isOpen) {
        $scope.isPlaylistOpened = !isOpen;
        $scope.selectedPlaylistIndex = index;
        $scope.selectedPlaylistId = playlist.Id;
        $scope.cantAddMessage = false;
    }

    $scope.removeSongFromPlaylist = function (song, playlist)
    {
        songService.removeSongFromPlaylist(song.Id, playlist.Id)
            .then(function (response) {
                var _playlist = _.findWhere($scope.playlists, { Id: playlist.Id });
                
                _playlist.Songs = _.without(_playlist.Songs, song);
            });
    }

    $scope.songSelected = function (index, song) {        

        if ($scope.selected != index) {
            $scope.selected = index;
            $scope.selectedSong = song;
           
        }
        else {
            $scope.selected = -1;
            $scope.selectedSong = null;
        }

        $scope.cantAddMessage = false;
    };

    $scope.addSongToPlaylist = function () {
        console.log("test");
        songService.songToPlaylist($scope.selectedSong.Id, $scope.selectedPlaylistId)
                   .then(function (response) {
                       console.log(response.data);
                       if (response.data) {
                           $scope.playlists[$scope.selectedPlaylistIndex].Songs.push($scope.selectedSong);
                       }
                       else {
                           $scope.cantAddMessage = true;
                       }
                   });
    }

    $scope.addPlaylist = function (newPlaylist) {
        playlistService.addPlaylist(newPlaylist)
            .then(function (response) {
                $scope.playlists.push(response.data);
                $scope.newPlaylist = "";
                $scope.isPlaylistOpened = false;

            })
    }
  
    $scope.removeSong = function (song) {
        songService.removeSong(song.Id)
            .then(function (response) {
                $scope.songs = _.without($scope.songs, song);

                $scope.playlistsWithSong = _.filter($scope.playlists, function (_playlist) {

                    return _.some(_playlist.Songs, function (_song) {                        
                        return _song.Id === song.Id;
                    });
                });
                console.log("test");
                console.log($scope.playlistsWithSong);
                angular.forEach($scope.playlistsWithSong, function (p) {                    
                    var _song = _.findWhere(p.Songs, { Id: song.Id });
                    console.log(_song);
                    p.Songs = _.without(p.Songs, _song); 
                });
            })
    }

    $scope.removePlaylist = function (playlist)
    {
        
        playlistService.removePlaylist(playlist.Id)
            .then(function (response) {
                $scope.playlists = _.without($scope.playlists, playlist);
                $scope.isPlaylistOpened = false;
            });
    }
}]);