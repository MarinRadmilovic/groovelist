﻿myApp.service('playlistService', ['$http', function ($http) {

    return {
        addPlaylist: function (playlist) {
            return $http.post('/api/playlist/add', playlist);
        },
        getAllPlaylists: function () {
            return $http.get('/api/playlist/all');
        },
        removePlaylist: function(playlistId){
            return $http({ url: "/api/playlist/remove", method: "Post", params: { playlistId: playlistId } });
        }
    };

}]);