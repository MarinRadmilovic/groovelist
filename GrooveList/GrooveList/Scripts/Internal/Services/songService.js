﻿myApp.service('songService', ['$http', function ($http) {

    return {
        addSong: function (song) {
            return $http.post("/api/songs/add", song);
        },
        getAllSongs: function () {
            return $http.get("/api/songs/all");
        },
        getSongById: function(songId){
            return $http({ url: "/api/songs/get", method: "GET", params: { songId: songId } });
        },
        editSong: function (song) {
            return $http.post("/api/song/edit", song);
        },
        songToPlaylist: function (songId, playlistId) {
            return $http.post("/api/songToPlaylist",  { SongId: songId, PlaylistId: playlistId });
        },
        removeSong: function (songId) {
            return $http({ url: "/api/songs/remove", method: "POST", params: { songId: songId } });
        },
        removeSongFromPlaylist: function (songId, playlistId) {
            return $http.post("/api/songs/removeFromPlaylist", { SongId: songId, PlaylistId: playlistId });
        }

    };
}]);