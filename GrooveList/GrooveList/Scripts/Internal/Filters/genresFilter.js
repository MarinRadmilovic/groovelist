﻿myApp.filter('genresFilter', function() {
    return function (input) {
        var genres = input.split(';');
        genres.pop();
        return genres.join(',');
    }
});