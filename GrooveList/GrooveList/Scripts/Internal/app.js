﻿'use strict';

var myApp = angular.module('myApp', ['ui.router', 'ui.bootstrap', 'ui.moment-duration', 'ng-context-menu']);

myApp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', function ($stateProvider, $locationProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);

    $stateProvider
        .state('home', {
            url: '/',
            views:{
                'navigation': {
                    templateUrl: 'Content/Templates/Navigation.html'
                },
                'content': {
                    templateUrl: 'Content/Templates/Home.html' 
                }
            }
        })
        .state('addSong', {
            url: '/addSong',
            views: {
                'navigation': {
                    templateUrl: 'Content/Templates/Navigation.html'
                },
                'content': {
                    templateUrl: 'Content/Templates/AddSong.html'
                }
            }
        })
        .state('editSong', {
            url: '/song/edit/{songId}',
            views: {
                'navigation': {
                    templateUrl: 'Content/Templates/Navigation.html'
                },
                'content': {
                    templateUrl: 'Content/Templates/EditSong.html'
                }
            },
            controller: function($stateParams){
                $stateParams.songId  //*** Exists! ***//
            }
        })

}]);