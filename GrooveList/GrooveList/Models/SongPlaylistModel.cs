﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrooveList.Models
{
    public class SongPlaylistModel
    {
        public int SongId { get; set; }
        public int PlaylistId { get; set; }
    }
}