﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrooveList.Models
{
    public class SongModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public string YoutubeURL { get; set; }
        public string Genres { get; set; }
        public string Duration { get; set; }
    }
}