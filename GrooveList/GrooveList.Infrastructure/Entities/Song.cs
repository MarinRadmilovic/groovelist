﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrooveList.Infrastructure.Entities
{
    public class Song
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public string Duration { get; set; }
        public string YoutubeURL { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public string Genres { get; set; }
        public List<Playlist> Playlists { get; set; }
    }
}
