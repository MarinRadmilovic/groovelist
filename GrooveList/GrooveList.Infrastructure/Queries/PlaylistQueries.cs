﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ent = GrooveList.Infrastructure.Entities;
using db = GrooveList.Database;

namespace GrooveList.Infrastructure.Queries
{
    public class PlaylistQueries
    {
        public List<ent.Playlist> GetAll()
        {
            using (var db = new db::GrooveListDbContainer()) 
            {
                return db.Playlists.Select(p => new ent.Playlist
                {
                    Id = p.Id,
                    Name = p.Name,
                    Songs = p.Songs.Select(s => new ent.Song {
                                                     Id = s.Id,
                                                     Name = s.Name,
                                                     Artist = s.Artist,
                                                     Duration = s.Duration,
                                                     Genres = s.Genres,
                                                     YoutubeURL = s.YoutubeURL
                                                }).ToList()

                                            }).ToList();
            }
                   
        } 
        public ent.Playlist GetPlaylistById(int songId)
        {
            return null;
        }
        public List<ent.Playlist> SearchPlaylists(string queryText)
        {
            return null;
        }
    }
}
