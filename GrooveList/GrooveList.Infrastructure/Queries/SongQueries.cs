﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ent = GrooveList.Infrastructure.Entities;
using db = GrooveList.Database;

namespace GrooveList.Infrastructure.Queries
{
    public class SongQueries
    {
        public ent.Song GetSongById(int songId) 
        {
            return null;
        }
        public List<ent.Song> SearchSongs(string queryText)
        {
            return null;
        }
        public List<ent.Song> GetAll() 
        {
            using( var db = new db::GrooveListDbContainer())
            {

                return db.Songs.Select(s => new ent.Song
                {
                    Id = s.Id,
                    Name = s.Name,
                    Artist = s.Artist,
                    Duration = s.Duration,
                    Genres = s.Genres,
                    YoutubeURL = s.YoutubeURL
                    //Playlists = s.Playlists.Select(p => new ent.Playlist { 
                    //                                    Id = p.Id,
                    //                                    Name = p.Name
                    //                              }).ToList()
                }).ToList();
            
            }
             
        }
    }
}
