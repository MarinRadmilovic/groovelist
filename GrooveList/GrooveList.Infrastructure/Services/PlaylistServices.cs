﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ent = GrooveList.Infrastructure.Entities;
using db = GrooveList.Database;

namespace GrooveList.Infrastructure.Services
{
    public class PlaylistServices
    {
        public ent.Playlist AddPlaylist(ent.Playlist playlist) 
        {
            using (var db = new db::GrooveListDbContainer()) 
            {
                var dbPlaylist = new db::Playlist{
                        Name = playlist.Name
                };
                db.Playlists.Add(dbPlaylist);
                db.SaveChanges();
                playlist.Id = dbPlaylist.Id;
                playlist.Songs = new List<ent.Song>();
                return playlist;
            }
        }
        public void RemovePlaylist(int playlistId)
        {
            using (var db = new db::GrooveListDbContainer())
            {
                var dbPlaylist = db.Playlists.SingleOrDefault(p => p.Id == playlistId);
                dbPlaylist.Songs.Clear();
                db.Playlists.Remove(dbPlaylist);               
                db.SaveChanges();
               
            }
        }
       
    }
}
