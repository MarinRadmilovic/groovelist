using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ent = GrooveList.Infrastructure.Entities;
using db = GrooveList.Database;
using GrooveList.Infrastructure.Queries;

namespace GrooveList.Infrastructure.Services
{
    public class SongServices
    {
        SongQueries songQueries;
        PlaylistQueries playlistQueries;
        public SongServices()
        {
            songQueries = new SongQueries();
            playlistQueries = new PlaylistQueries();
        }
        public ent.Song AddSong(ent.Song song) 
        {
            using (var db = new db::GrooveListDbContainer()) 
            {
                var dbSong = new db::Song { 
                    Name = song.Name,
                    Artist = song.Artist,
                    DateCreated = song.DateCreated,
                    Duration = song.Duration,
                    Genres = song.Genres,
                    YoutubeURL = song.YoutubeURL
                 
                };
                db.Songs.Add(dbSong);
                db.SaveChanges();

                song.Id = dbSong.Id;
                return song;
            }
            
        }
        public bool EditSong(ent.Song song)
        {
            using (var db = new db::GrooveListDbContainer())
            {
                try
                {
                    var dbSong = db.Songs.SingleOrDefault(s => s.Id == song.Id);

                    dbSong.Name = song.Name;
                    dbSong.Artist = song.Artist;
                    dbSong.Duration = song.Duration;
                    dbSong.Genres = song.Genres;
                    dbSong.YoutubeURL = song.YoutubeURL;

                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }

            }

        }
        public bool SongToPlaylist(int songId, int playlistId) 
        {
            using (var db = new db::GrooveListDbContainer()) 
            {
                var dbSong = db.Songs.SingleOrDefault(s => s.Id == songId);
                var dbPlaylist = db.Playlists.SingleOrDefault(p => p.Id == playlistId);
                if (!dbPlaylist.Songs.Contains(dbSong)) 
                {
                    dbPlaylist.Songs.Add(dbSong);
                    db.SaveChanges();
                    return true;
                }
                return false;
                
            }
        }
        public void RemoveSong(int songId) 
        {
            using (var db = new db::GrooveListDbContainer()) 
            {
                var dbSong = db.Songs.SingleOrDefault(s => s.Id == songId);
                dbSong.Playlists.Clear();
                db.Songs.Remove(dbSong);
                db.SaveChanges();
            }
        }

        public void RemoveSongFromPlaylist(int songId, int playlistId) 
        {
            using (var db = new db::GrooveListDbContainer())
            {
                var dbSong = db.Songs.SingleOrDefault(s => s.Id == songId);
                db.Playlists.SingleOrDefault(p => p.Id == playlistId).Songs.Remove(dbSong);

                db.SaveChanges();
            }
        }

        public ent.Song GetSongById(int songId)
        {
            using (var db = new db::GrooveListDbContainer()) 
            {               
                var dbSong = db.Songs.SingleOrDefault(s => s.Id == songId);
                return new ent.Song{
                    Name = dbSong.Name,                    
                    Artist = dbSong.Artist,
                    DateCreated = dbSong.DateCreated,
                    Duration = dbSong.Duration,
                    Genres = dbSong.Genres,
                    YoutubeURL = dbSong.YoutubeURL,
                    Id = dbSong.Id,
                    Playlists = dbSong.Playlists.Select(p => new ent.Playlist
                    {
                        Id = p.Id,
                        Name = p.Name
                    }).ToList()
                };
            }
        }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           